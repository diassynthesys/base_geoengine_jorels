/* ---------------------------------------------------------
 * Odoo base_geoengine_jorels
 * Author Yannick Vaucher Copyright 2018 Camptocamp SA
 * License in __manifest__.py at root level of the module
 * ---------------------------------------------------------
 */
odoo.define('base_geoengine_jorels._view_registry', function (require) {
    "use strict";

    var GeoengineView = require('base_geoengine_jorels.GeoengineView');
    var view_registry = require('web.view_registry');

    view_registry.add('geoengine', GeoengineView);
});
