# base_geoengine_jorels

Odoo base_geoengine fork by Jorels, Jorge Sanabria
--------------------------------------------------

Include xyz custom OSM. Use for example:
    
    <record id="geoengine_raster_layer_travel_routeosm" model="geoengine.raster.layer">
    
        <field name="raster_type">xyz</field>
        
        <field name="name">Open XYZ Map</field>
        
        <field name="view_id" ref="ir_ui_view_travel_routeview0"/>
        
        <field eval="0" name="overlay"/>
        
        <field name="url">http://tile.example.org/{z}/{x}/{y}.png</field>
        
    </record>


===========================
Geospatial support for Odoo
===========================

Geospatial support based on PostGIS add the ability of server to server
geojson to do geo CRUD and view definition.

**Table of contents**

.. contents::
   :local:

Installation
============


To install this module, you need to have `PostGIS <http://postgis.net/>`_ installed.

On Ubuntu::

  sudo apt-get install postgis

The module also requires two additional python libs:

* `Shapely <http://pypi.python.org/pypi/Shapely>`_

* `geojson <http://pypi.python.org/pypi/geojson>`_

for a complete documentation please refer to the `public documenation <http://oca.github.io/geospatial/index.html>`_

Credits
=======

Authors
~~~~~~~

* Camptocamp
* ACSONE SA/NV

Contributors
~~~~~~~~~~~~

* Nicolas Bessi <nicolas.bessi@camptocamp.com>
* Frederic Junod <frederic.junod@camptocamp.com>
* Yannick Vaucher <yannick.vaucher@camptocamp.com>
* Sandy Carter <sandy.carter@savoirfairelinux.com>
* Laurent Mignon <laurent.mignon@acsone.eu>
* Jonathan Nemry <jonathan.nemry@acsone.eu>
* David Lasley <dave@dlasley.net>
* Daniel Reis <dgreis@sapo.pt>
* Matthieu Dietrich <matthieu.dietrich@camptocamp.com>
* Alan Ramos <alan.ramos@jarsa.com.mx>
* Damien Crier <damien.crier@camptocamp.com>
* Cyril Gaudin <cyril.gaudin@camptocamp.com>
* Pierre Verkest <pverkest@anybox.fr>
* Benjamin Willig <benjamin.willig@acsone.eu>
* Devendra Kavthekar <dkatodoo@gmail.com>
* Emanuel Cino <ecino@compassion.ch>
* Thomas Nowicki <thomas.nowicki@camptocamp.com>
* Alexandre Saunier <alexandre.saunier@camptocamp.com>
* Sandip Mangukiya <smangukiya@opensourceintegrators.com>
* Jorge Sanabria <js@jorels.com>

Maintainers
~~~~~~~~~~~

The base_geoengine module is maintained by the OCA, and base_geoengine_jorels is a fork of it.

.. image:: https://odoo-community.org/logo.png
   :alt: Odoo Community Association
   :target: https://odoo-community.org

OCA, or the Odoo Community Association, is a nonprofit organization whose
mission is to support the collaborative development of Odoo features and
promote its widespread use.

The base_geoengine module is part of the `OCA/geospatial <https://github.com/OCA/geospatial/tree/12.0/base_geoengine>`_ project on GitHub.

You are welcome to contribute. To learn how please visit https://odoo-community.org/page/Contribute.
